# Independent Contractor Agreement

## Parties

This Agreement is made between

&nbsp;

(hereinafter "Contractor")
with a principal place of business/address at

&nbsp;

and **Viper Development UG (haftungsbeschränkt)**, Haakestrasse 37,
21075 Hamburg, VAT ID DE312932940 (hereinafter "Company").

## Services to be Performed

The Contractor will be offered projects with a given time budget (number of
billable hours) in writing (e.g. GitLab, mail or chat).

The Contractor may accept working on the project and must use the time by
working to complete it.

The Contractor may not delegate the work to any third party without written
(e.g. mail or chat) consent.

## Payment

All numbers are excluding VAT.

The Contractor will be paid **Eur __** per hour worked on projects in
question as agreed.

## Expenses

The Contractor will not charge the Company for own tooling used to develop
the application or any other expenses unless explicitly agreed upon.

## Invoices

The Contractor may submit invoices for the services rendered at any date of his
discretion. The Company will pay the Contractor within 14 days after receipt of
each invoice.

## Termination

The agreement may be terminated by any party at any time for any reason. Upon
termination, the Contractor has to provide all data and/or uncompiled source
code related to the project via email within 14 days. The Contractor may not
bill any hours beyond the termination.

### Currency and Conversion

Invoices will be paid in Euro. The Contractor is responsible for covering
payment and conversion fees and is advised to use services like Transferwise for
transactions to other currencies.

### Late Payments

If no payment arrives on the 16th day after receipt of the invoice at 11am CET,
a late payment fee of 5% will be added to the amount. It will be readded every
16 days if the full amount is not covered.

## Intellectual Property Ownership

The Contractor grants the Company a royalty-free nonexclusive perpetual
license to use source code, designs, documents or other work created or
developed by the Contractor for the Company under this agreement. This does
not apply to source code created for open source projects.

A non disclosure agreement will be provided separately.

## Provisions Separable

The provisions of this Agreement are independent of and separable from each
other, and no provision shall be affected or rendered invalid or unenforceable
by virtue of the fact that for any reason any other or others of them may be
invalid or unenforceable in whole or in part.

## Governing Law

This Agreement will be governed by and construed in accordance with the laws of
Germany.

## Partnership

The Agreement does not create a partnership relationship. The Contractor
does not have authority to enter into contracts on Company's behalf.

The Contractor's relationship with the Company will be that of an
independent contractor and not that of an employee.

## Amendment

All the amendments made to this contract shall be made on written with the
formal consent of both the parties signing off.

# Signatures

## Company:

&nbsp;

Date, Signature:

&nbsp;

Name, Company: Lasse Schuirmann, CEO of Viper Development UG

&nbsp;

## Contractor:

&nbsp;

Date, Signature:

&nbsp;

Name, Company:
